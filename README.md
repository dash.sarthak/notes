---
title: NOTES APP
description: A localized markdown notes app.
created_at: 8 October, 2023
---

# NOTES APP
## About
- A markdown editor app that is used from the browser.
- All the "vaults" are folders stored locally, all the "notes" are markdown files stored locally.
- There will be a duckdb file that will store the schema/structure of your notes locally. This file can be synced across machines to have a consistent notes directory across multiple devices (mobile not supported).

## Technology used
- golang
- duckdb
- htmx

